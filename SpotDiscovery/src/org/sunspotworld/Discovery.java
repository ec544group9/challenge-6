/*
 * SunSpotApplication.java
 *
 * Created on Nov 15, 2012 1:44:50 AM;
 */
package org.sunspotworld;

import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.TimeoutException;
import com.sun.spot.peripheral.radio.IProprietaryRadio;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.peripheral.radio.RadioFactory;
//import com.sun.spot.peripheral.radio.routing.RoutingPolicyManager;
//import com.sun.spot.peripheral.radio.mhrp.aodv.AODVManager;
//import com.sun.spot.peripheral.radio.shrp.SingleHopManager;
import com.sun.spot.util.IEEEAddress;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ILed;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.IAccelerometer3D;
import com.sun.spot.io.j2me.radiogram.Radiogram;
import com.sun.spot.io.j2me.radiogram.RadiogramConnection;

import com.sun.spot.util.Utils;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * Routines to turn multiple SPOTs broadcasting their information and discover
 * peers in the same PAN
 *
 * The SPOT uses the LEDs to display its status as follows:
 * LED 0:
 * Red = missed an expected packet
 * Green = received a packet
 *
 * LED 1-5:
 * Leader displayed count of connected spots in green
 * 
 * LED 6:
 * display TiltChange flag when sw1 is pressed
 * Blue = false
 * Green = true
 * 
 * LED 7:
 * Red = right tilt 
 * Blue = left tilt
 *
 * Press right witch to change neighbors' tilt state:
 * by sending out tilt change flag in the datagram
 * SW1 = change neighbors' tilt
 * 
 * Switch 2 right now is used to adjust transmitting power
 *
 * Note: Each group need to use their own channels to avoid interference from others
 * channel 26 is default
 * Group 1: 11-13
 * Group 2: 14-16
 * Group 4: 20-22
 * Group 5: 23-25
 * Group 6: 17-19
 * 
 * 
 * @author Yuting Zhang
 * date: Nov 28,2012
 * Note: this work is base on Radio Strength demo from Ron Goldman
 */
public class Discovery extends MIDlet
{

    private static final String VERSION = "1.0";
    // CHANNEL_NUMBER  Setting this to channel 19 for Group 9
    private static final int CHANNEL_NUMBER = 19;
    // Forcing the PAN ID to be 9 for our group
    private static final short PAN_ID = 9;
    private static final String BROADCAST_PORT = "38"; /* Broadcasting from a unique port */

    private static final int PACKETS_PER_SECOND = 1;
    private static final int PACKET_INTERVAL = 3000 / PACKETS_PER_SECOND;
    //   private static AODVManager aodv = AODVManager.getInstance();
    private int channel = CHANNEL_NUMBER;
    private int power = 32;                             // Start with max transmit power
    private ISwitch sw1 = (ISwitch) Resources.lookup(ISwitch.class, "SW1");
    private ISwitch sw2 = (ISwitch) Resources.lookup(ISwitch.class, "SW2");
    private ITriColorLEDArray leds = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    private ITriColorLED statusLED = leds.getLED(0);
    private ITriColorLED leaderLED = leds.getLED(1);
    private IAccelerometer3D accel = (IAccelerometer3D) Resources.lookup(IAccelerometer3D.class);
//    private ILightSensor light = (ILightSensor)Resources.lookup(ILightSensor.class);
    private LEDColor red = new LEDColor(50, 0, 0);
    private LEDColor green = new LEDColor(0, 50, 0);
    private LEDColor blue = new LEDColor(0, 0, 50);
    private double Xtilt;
    private boolean xmitDo = true;
    private boolean recvDo = true;
    private boolean ledsInUse = false;
    private long myAddr = 0; // own MAC addr (ID)
    private long leader = 0;  // leader MAC addr 
    private long follower = 0; // follower MAC addr
    private long TimeStamp;
    private int tilt = 0; // initialized as 0, right == 1, left == -1
    private boolean tiltchange = false;

    private SpotNetwork leaderList;
    private boolean command=false;
    private String myAddrString = "";
    private boolean infect=false;
    private boolean sentImHere = false;
    private boolean infected = false;

    private static final int LEADER_CODE = 1;
    private static final int LEADER_SET_CODE = 2;
    private static final int INFECTION_CODE = 3;
    private static final int IM_HERE_CODE = 4;
    
    /* This is global since the receive loop may send a message */
    RadiogramConnection txConn = null;

    /* The number of connected nodes */
    private int numConnectedSpots = 0;

    private void accelLoop()
    {
        while(true)
        {
            try
            {
                Xtilt = accel.getTiltX();
                if (command == true)
                {
                    leds.getLED(7).setOn();
                }
                else
                {
                    if (Xtilt > 0)
                    {
                        tilt = 1;  
                        leds.getLED(7).setColor(red);
                        leds.getLED(7).setOn();
                    }
                    else if (Xtilt < 0)
                    {
                        tilt = -1;  
                        leds.getLED(7).setColor(blue);
                        leds.getLED(7).setOn();
                    }
                }
            }
            catch (Exception e)
            {
                System.err.println("Accelerometer read error...");
            }
        }
    }
    /**
     * Loop to continually broadcast message.
     * message format
     * (long)myAddr,(long)follower,(long)leader,(long)TimeStamp,(int)tilt,
     * (boolean)tiltchang,(int)power,(int)count
     */
    private void xmitLoop()
    {
        ILed led = Spot.getInstance().getGreenLed();
        
        xmitDo = true;
        while (xmitDo)
        {
            try
            {
                txConn = (RadiogramConnection) Connector.open("radiogram://broadcast:" + BROADCAST_PORT);
                txConn.setMaxBroadcastHops(1);      // don't want packets being rebroadcasted
                Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());
                long count = 0;
                boolean ledOn = false;
   
                while (xmitDo)
                {
                    for (int ii = 2 ; ii < 7 ; ii++)
                        leds.getLED(ii).setOff();
                    
                    /* Now force yourself to be active just in case you were removed as the leader for some reason */
                    leaderList.changeSpotState(myAddrString, true);
                    if (!leaderList.isLeader(myAddrString))
                    {
                        /* Display a light that is yellow if not the leader */
                        leaderLED.setColor(LEDColor.YELLOW);
                        leaderLED.setOn();
                        continue;
                    }
                    
                    leaderLED.setColor(LEDColor.CYAN);
                    leaderLED.setOn();
                    
                    System.out.println("Transmitting leader message");
                    TimeStamp = System.currentTimeMillis();
                    
                    
                    count++;
                    if (count >= Long.MAX_VALUE)
                    {
                        count = 0;
                    }
                    xdg.reset();
                    xdg.writeInt(LEADER_CODE); // Simply indicates that this is a leader message
                    xdg.writeLong(myAddr); // own MAC address
                    xdg.writeLong(leader); // own leader's MAC address
                    xdg.writeLong(TimeStamp); // current timestamp
                    //                  xdg.writeInt(STEER); // locl STEER amount following leader 
                    //                  xdg.writeInt(SPEED); // local SPEED amount following leader
                    xdg.writeLong(count); // local count
                    txConn.send(xdg);

                    /* Finall set the number of connected nodes since the last check */
                    for (int ii = 2 ; ii < 7 ; ii++)
                        leds.getLED(ii).setOff();
                    
                    numConnectedSpots = numConnectedSpots < 6 ? numConnectedSpots : 0;
                    for (int ii = 2 ; ii < 2 + numConnectedSpots ; ii++)
                    {
                        leds.getLED(ii).setColor(LEDColor.WHITE);
                        leds.getLED(ii).setOn();
                    }
                    numConnectedSpots = 0; /* Reset the count */
                    
                    long delay = (TimeStamp + PACKET_INTERVAL - System.currentTimeMillis()) - 2;
                    if (delay > 0)
                    {
                        pause(delay);
                    }
                }
            }
            catch (IOException ex)
            {
                // ignore
            }
            finally
            {
                if (txConn != null)
                {
                    try
                    {
                        txConn.close();
                    }
                    catch (IOException ex)
                    {
                    }
                }
            }
        }
    }

    /**
     * Loop to receive packets and discover peers information
     * (long)srcAddr,(long)srcLeader,(long)srcFollow,(long)srcTime,(int)srcSTEER,(int)srcSPEED
     * 
     * [TO DO]
     * sort out leader-follower by their MAC address order 
     * very most leader needs to know himself as the leader, then launch movement
     * very most follower needs to know himself as the last 
     */
    private void recvLoop()
    {
        ILed led = Spot.getInstance().getRedLed();
        RadiogramConnection rcvConn = null;
        recvDo = true;
        int nothing = 0;
        while (recvDo)
        {
            try
            {
                System.err.println("Checking for messages");
                rcvConn = (RadiogramConnection) Connector.open("radiogram://:" + BROADCAST_PORT);
                rcvConn.setTimeout(PACKET_INTERVAL - 5);
                Radiogram rdg = (Radiogram) rcvConn.newDatagram(rcvConn.getMaximumLength());
                long count = 0;
                boolean ledOn = false;
                while (recvDo)
                {
                    try
                    {
                        rdg.reset();
                        rcvConn.receive(rdg);           // listen for a packet
                        long srcAddr; // src MAC address
                        long srcLeader; // src's leader
                        long srcFollow; // src's follow
                        long srcTime; // src's timestamp
                        long srcCount;
                        int srcTilt;

                        int code = rdg.readInt();                        
                        if (code == LEADER_CODE)
                        {
                            /* The standard case, this is a leader message from another spot */
                            srcAddr = rdg.readLong(); // src MAC address
                            srcLeader = rdg.readLong(); // src's leader
                            srcTime = rdg.readLong(); // src's timestamp
                            String srcID = IEEEAddress.toDottedHex(srcAddr);
                            
                            /* Now add this spot to the network, but first disable the old leader under the 
                             * assumption that the old leader is gone.  if the old leader is not gone, we will
                             * just make him leader again on the next turn, while the other spot because re enabled
                             * with the i'm here message.
                             */
                            if (!leaderList.isLeader(myAddrString) && !leaderList.isLeader(srcID))
                                leaderList.changeSpotState(leaderList.getCurrentLeader(), false);
                            
                            if (!leaderList.isLeader(srcID))
                            {
                                leaderList.addSpot(srcID);
                            }
                            else
                            {
                                /* Send an Im Here message */
                                System.out.print("Sending im here...");
                                Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());
                                xdg.reset();
                                xdg.writeInt(IM_HERE_CODE); // Simply indicates that this is a leader message
                                xdg.writeLong(myAddr); // own MAC address
                                txConn.send(xdg);
                                System.out.println("Im here sent...");
                            }

                            srcCount = rdg.readLong();
                            System.out.println("Received Datagram from " + srcID);
                            
                            led.setOn();
                            statusLED.setColor(green);
                            statusLED.setOn();
                        }
                        else if (code == INFECTION_CODE)
                        {
                            System.err.println("Received infection code...");
                            srcFollow=rdg.readLong(); // own MAC address
                            srcTilt=rdg.readInt();
                            srcTime=rdg.readLong();
                            System.out.println("Infecting " + IEEEAddress.toDottedHex(srcFollow) + " who is " + infected + " " + srcTilt);
                            
                            if (!infected)
                            {      
                                if(srcFollow==myAddr || srcFollow==0){
                                    if (srcTilt==1)
                                    {
                                        command=true;
                                        leds.getLED(7).setColor(red);
                                        leds.getLED(7).setOn();
                                    }
                                    else
                                    {
                                        command=true;
                                        leds.getLED(7).setColor(blue);
                                        leds.getLED(7).setOn();
                                    }

                                    if(srcFollow==myAddr){
                                        pause(3000);
                                        follower = IEEEAddress.toLong(leaderList.getNextNode(myAddrString));
                                        sendButton(srcTilt,srcFollow);

                                    }
                                    infected = true;
                                }
                            }
                        }
                        else if (code == IM_HERE_CODE)
                        {
                            statusLED.setColor(green);
                            statusLED.setOn();
                            
                            /* Get address of the fellow nodes in the network */
                            srcAddr = rdg.readLong();
                            String srcID = IEEEAddress.toDottedHex(srcAddr);
                            leaderList.addSpot(srcID);
                            
                            /* If currently the leader, count the number of messages received */
                            if (leaderList.isLeader(myAddrString))
                                numConnectedSpots += 1;
                        }
                        else
                        {
                            statusLED.setColor(red);
                            statusLED.setOn();
                        }

                        
                        //tiltchange=srcTiltChange;
                    
                        
                                
                        /**
                         * [TO DO]
                         * insert script to sort leader-follower relation
                         * neighbors change tilt according to sreTiltChangeFlag
                         */
                    }
                    catch (TimeoutException tex)
                    {        // timeout - display no packet received
                        if (leaderList.isLeader(myAddrString))
                        {
                            statusLED.setColor(green);
                            statusLED.setOn();
                            continue; /* I am the leader, i don't expect a message unless its an infection */
                        }
                        
                        if (leaderList.getNetworkSize() == 1)
                        {
                            /* The current is the only node in the network, don't want to remove self */
                            System.out.println("No other nodes in the network currently...");
                            statusLED.setColor(LEDColor.WHITE);
                            statusLED.setOn();
                        }
                        else
                        {
                            System.out.println("Did not receive a message from the leader, removing from network!");
                            statusLED.setColor(red);
                            statusLED.setOn();
                            nothing++;
                            if (nothing > 2 * PACKETS_PER_SECOND && !ledsInUse)
                            {
                                for (int ledint = 0; ledint <= 7; ledint++)
                                { // if nothing received eventually turn off LEDs
                                    leds.getLED(ledint).setOff();
                                }
                            }

                            leaderList.changeSpotState(leaderList.getCurrentLeader(), false);
                           
                        }
                        numConnectedSpots = 0;
                    }
                }
            }
            catch (IOException ex)
            {
                // ignore
            }
            finally
            {
                if (rcvConn != null)
                {
                    try
                    {
                        rcvConn.close();
                    }
                    catch (IOException ex)
                    {
                    }
                }
            }
        }
    }

    /**
     * Pause for a specified time.
     *
     * @param time the number of milliseconds to pause
     */
    private void pause(long time)
    {
        try
        {
            Thread.currentThread().sleep(time);
        }
        catch (InterruptedException ex)
        { /* ignore */ }
    }

    /**
     * Initialize any needed variables.
     */
    private void initialize()
    {
        myAddr = RadioFactory.getRadioPolicyManager().getIEEEAddress();
        myAddrString = IEEEAddress.toDottedHex(myAddr);
        statusLED.setColor(red);     // Red = not active
        statusLED.setOn();
        IRadioPolicyManager rpm = Spot.getInstance().getRadioPolicyManager();
        rpm.setChannelNumber(channel);
        rpm.setPanId(PAN_ID);
        rpm.setOutputPower(power - 32);
        //    AODVManager rp = Spot.getInstance().
        
        /* Init the leader list */
        leaderList = new SpotNetwork(myAddrString);
    }

    /**
     * Main application run loop.
     */
    private void run()
    {
        System.out.println("Radio Signal Strength Test (version " + VERSION + ")");
        System.out.println("Packet interval = " + PACKET_INTERVAL + " msec");
        
//        /* Run the tests */
//        SpotNetwork spotNetwork = new SpotNetwork(IEEEAddress.toDottedHex(myAddr));
//        spotNetwork.runTests();

        /* Button and infection gets max priority */
        new Thread()
        {
            public void run()
            {
                accelLoop();
            }
        }.start();
        new Thread()
        {

            public void run()
            {
                xmitLoop();
            }
        }.start();                      // spawn a thread to transmit packets
        new Thread()
        {

            public void run()
            {
                recvLoop();
            }
        }.start();                      // spawn a thread to receive packets
        respondToSwitches();            // this thread will handle User input via switches
    }

    /**
     * Display a number (base 2) in LEDs 1-7
     *
     * @param val the number to display
     * @param col the color to display in LEDs
     */
    private void displayNumber(int val, LEDColor col)
    {
        for (int i = 0, mask = 1; i < 7; i++, mask <<= 1)
        {
            leds.getLED(7 - i).setColor(col);
            leds.getLED(7 - i).setOn((val & mask) != 0);
        }
    }

    /**
     * Loop waiting for user to press a switch.
     *<p>
     * Since ISwitch.waitForChange() doesn't really block we can loop on both switches ourself.
     *<p>
     * Detect when either switch is pressed by displaying the current value.
     * After 1 second, if it is still pressed start cycling through values every 0.5 seconds.
     * After cycling through 4 new values speed up the cycle time to every 0.3 seconds.
     * When cycle reaches the max value minus one revert to slower cycle speed.
     * Ignore other switch transitions for now.
     *
     */
    private void respondToSwitches()
    {
        while (true)
        {
            
            /*there are two control signal to realization button B*/
            
            if(sw1.isClosed()){
                System.out.println("Button A");
                infected = false;
                command = !command;
            }
            else if (sw2.isClosed())
            {
                System.out.println("Button B");
                //tiltchange=true;
                if (!leaderList.isLeader(myAddrString))
                {
                    System.out.println("This is a follower command");
                    infect=true;
                }
                
                System.out.println("B1");

                if (Xtilt > 0)
                {
                    tilt = 1;  


                }
                else if (Xtilt <= 0)
                {
                    tilt = -1;  

                }

                System.out.println("B2");
                if(infect){
                    follower = IEEEAddress.toLong(leaderList.getNextNode(myAddrString));
                }
                else{
                    follower=0;
                }
                command = true;
                System.out.println("B3");
                sendButton(tilt,follower);
                
                infect = false;
            }  
            
            pause(50);
        }
    }
    
    private void sendButton(int tilt, long follow)
    {
        ILed led = Spot.getInstance().getGreenLed();
        RadiogramConnection txConn = null;
        
        try
        {
            txConn = (RadiogramConnection) Connector.open("radiogram://broadcast:" + BROADCAST_PORT);
            txConn.setMaxBroadcastHops(1);      // don't want packets being rebroadcasted
            Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());
            long count = 0;
            boolean ledOn = false;
            
            System.out.println("Sending infect/leaderset message with " + tilt + " " + follow);
            led.setOn();
            TimeStamp = System.currentTimeMillis();

           
//            count++;
//            if (count >= Long.MAX_VALUE)
//            {
//                count = 0;
//            }
            xdg.reset();
            xdg.writeInt(INFECTION_CODE); // Simply indicates that this is a leader message
            xdg.writeLong(follower); // own MAC address
            xdg.writeInt(tilt);
//            xdg.writeLong(leader); // own leader's MAC address
            xdg.writeLong(TimeStamp); // current timestamp
            //                  xdg.writeInt(STEER); // locl STEER amount following leader 
            //                  xdg.writeInt(SPEED); // local SPEED amount following leader
//            xdg.writeLong(count); // local count
            txConn.send(xdg);
            led.setOff();
            long delay = (TimeStamp + PACKET_INTERVAL - System.currentTimeMillis()) - 2;
            if (delay > 0)
            {
                pause(delay);
            }
            leds.getLED(7).setOff();
        }
        catch (IOException ex)
        {
            // ignore
        }
        finally
        {
            if (txConn != null)
            {
                try
                {
                    txConn.close();
                }
                catch (IOException ex)
                {
                }
            }
        }
    }

    /**
     * MIDlet call to start our application.
     */
    protected void startApp() throws MIDletStateChangeException
    {
        // Listen for downloads/commands over USB connection
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        initialize();
        run();
    }

    /**
     * This will never be called by the Squawk VM.
     */
    protected void pauseApp()
    {
        // This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException
    {
    }
}