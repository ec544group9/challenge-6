/*
 * Class which manages all spots that have, at any given point, been connected
 * to the current SPOT, and tracks whether they are currently connected.  At
 * request, returns who the current leader is.
 */
package org.sunspotworld;

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Vector;

import com.sun.spot.util.IEEEAddress;
/**
 *
 * @author Raphy
 */
public class SpotNetwork
{
    /* Class which stores the ID and whether it is active or not (Map string to boolean) */
    private Hashtable connectedSpots = new Hashtable();
    private Hashtable activeSpots = new Hashtable(); /* The active spots on a given roll call */
    private String currentLeader;

    /* Always instantiated with itself */
    public SpotNetwork(String selfID)
    {
        connectedSpots.put(selfID, Boolean.TRUE);
        currentLeader = selfID;
    }
    
    public void runTests()
    {
        /* Runs tests on the current class to ensure proper functionality */
        System.out.println(getCurrentLeader());
        addSpot("0014.4F01.0000.45A9");
        System.out.println(getCurrentLeader());
        changeSpotState("0014.4F01.0000.45A9", false);
        System.out.println(getCurrentLeader());
        changeSpotState("0014.4F01.0000.45A9", true);
        System.out.println(getCurrentLeader());
        addSpot("0014.4F01.0000.7D2E");
        System.out.println(getCurrentLeader()); 
        
        System.out.println("\nNext node test...\n");
        
        System.out.println("Starting with 0014.4F01.0000.7D2E");
        String temp = getNextNode("0014.4F01.0000.7D2E");
        for (int ii = 0 ; ii < 5 ; ii++)
        {
            System.out.println("The next node is " + temp);
            temp = getNextNode(temp);
        }
    }
    
    /**
     * @return Returns the size of the network
     */
    public int getNetworkSize()
    {
        return connectedSpots.size();
    }
    
    /**
     * @return The current leader in the network
     */
    public String getCurrentLeader()
    {
        return currentLeader;
    }
    
    /**
     * @return True if the input is the leader
     */
    public boolean isLeader(String ID)
    {
        return ID.equals(currentLeader);
    }
    
    /**
     * Gets the next follower.  If it returns null, it is the only active node
     * in the network
     */
    public String getNextNode(String ID)
    {
        String nextKey = "";
        String toCheck = "";
        String smallest = "";
        long MAX_Address = Long.MAX_VALUE;
        long smallestVal = Long.MAX_VALUE;
        long nextKeyVal = MAX_Address;
        
        Enumeration keys = connectedSpots.keys();
        while (keys.hasMoreElements())
        {
            toCheck = (String) keys.nextElement();
            if (ID.equals(nextKey))
            {
                continue;
            }
            else
            {
                long IDVal = IEEEAddress.toLong(ID);
                long toCheckVal = IEEEAddress.toLong(toCheck);
                /* If its smaller than the potential, but bigger that the ID, update */
                if (toCheckVal > IDVal && toCheckVal < nextKeyVal)
                {
                    nextKey = toCheck;
                    nextKeyVal = toCheckVal;
                }    
                
                if (toCheckVal < smallestVal)
                {
                    smallestVal = toCheckVal;
                    smallest = toCheck;
                }
            }
        }
        
        /* Return next Key if possible, otherwise return smallest. If no smaller, return null */
        if (nextKey.equals(""))
        {
            /* There was none bigger, so assign the smallest instead */
            if (!smallest.equals("") && !smallest.equals(ID))
            {
                return smallest;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return nextKey;
        }
    }
    
    /**
     * Adds the given address to the connected spots as active, and updates
     * the current leader, or activates the node again
     * @param ID 
     */
    public void addSpot(String ID)
    {
        if (!connectedSpots.containsKey(ID))
        {
            System.out.println("The original leader is " + getCurrentLeader());
            connectedSpots.put(ID, Boolean.TRUE);
            updateCurrentLeader();
            System.out.println("Added spot, the new leader is " + getCurrentLeader());
        }
        else if ( (Boolean) connectedSpots.get(ID) == Boolean.FALSE)
        {
            changeSpotState(ID, true);
            updateCurrentLeader();
        }
    }
    
    /**
     * Disables the address specified for the network.  The address will no longer
     * be used for leader determination.  The current leader is also updated.
     * @param ID
     * @return True if was successful, or false if the ID is not found in the network
     */
    public boolean changeSpotState(String ID, boolean state)
    {
        if (!connectedSpots.containsKey(ID))
        {
            return false;
        }
        connectedSpots.put(ID, Boolean.valueOf(state));
        updateCurrentLeader();
        return true;
    }
    
    public void printList()
    {
        System.out.println("\nThe node network...\n");
        Enumeration keys = connectedSpots.keys();
        while (keys.hasMoreElements())
        {
            String ID = (String) keys.nextElement();
            System.out.println(ID);
        }
        System.out.println();
    }
    
    /**
     * Iterates over connected spots to determine who the current leader is
     */
    private void updateCurrentLeader()
    {
        long currentLeaderValue = 0;
        Enumeration keys = connectedSpots.keys();
        while (keys.hasMoreElements())
        {
            String ID = (String) keys.nextElement();
            if (connectedSpots.get(ID) == Boolean.TRUE)
            {
                /* Active ID, compare to the current leader */
                long value = IEEEAddress.toLong(ID);
                if (value > currentLeaderValue)
                {
                    currentLeader = ID;
                    currentLeaderValue = value;
//                    System.out.println("Leader updated to " + currentLeader + " and state was " + connectedSpots.get(ID));
                }
            }
        }
    }
}
